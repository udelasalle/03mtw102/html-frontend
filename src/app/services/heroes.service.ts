import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

// API_BASE_URL

@Injectable({
  providedIn: 'root'
})
export class HeroesService {

  constructor(private http: HttpClient) { }

  getMarvelHeroes() {
    return this.http.get(`${environment.API_BASE_URL}/marvel`);
  }
  getDcHeroes() {
    return this.http.get(`${environment.API_BASE_URL}/dc`);
  }

  getAbout() {
    return this.http.get(`${environment.API_BASE_URL}/about`);
  }
}
