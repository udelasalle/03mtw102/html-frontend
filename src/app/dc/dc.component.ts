import { Component, OnInit } from '@angular/core';
import { HeroesService } from '../services/heroes.service';

@Component({
  selector: 'app-dc',
  templateUrl: './dc.component.html',
  styleUrls: ['./dc.component.css']
})
export class DcComponent implements OnInit {

  constructor(private heroesSvc: HeroesService) { }

  dataDc: any = [];
  ngOnInit(): void {
    this.heroesSvc.getDcHeroes().subscribe((data: any) => {
      // Datos de Heroes de Dc
      console.log(data);
      this.dataDc = data.data;
    });
  }

}
